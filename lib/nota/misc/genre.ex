defmodule Nota.Misc.Genre do
  use Ecto.Schema
  import Ecto.Changeset


  schema "genres" do
    field :image, :string
    field :name, :string
    has_many :stories, Nota.Stories.Story
    timestamps()
  end

  @doc false
  def changeset(genre, attrs) do
    genre
    |> cast(attrs, [:name, :image])
    |> validate_required([:name, :image, :stories])
  end
end
