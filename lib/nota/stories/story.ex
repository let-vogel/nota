defmodule Nota.Stories.Story do
  use Ecto.Schema
  import Ecto.Changeset


  schema "stories" do
    field :description, :string
    belongs_to :genre, Nota.Misc.Genre
    field :title, :string

    timestamps()
  end

  @doc false
  def changeset(story, attrs) do
    story
    |> cast(attrs, [:title, :description])
    |> validate_required([:title, :description])
    |> assoc_constraint(:genre)
  end
end
