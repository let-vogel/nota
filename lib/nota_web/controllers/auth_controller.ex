defmodule NotaWeb.AuthController do
    use NotaWeb, :controller
    plug Ueberauth
    alias Nota.Accounts
  
    def callback(%{assigns: %{ueberauth_auth: auth}} = conn, params) do
      IO.inspect auth
      user_params = %{
        token: auth.credentials.token,
        email: auth.info.email,
        provider: "google"
      }
      changeset = Accounts.User.changeset(%Accounts.User{}, user_params)
  
      signin(conn, changeset)
    end
  
    def signout(conn, _params) do
      conn
      |> configure_session(drop: true)
      |> redirect(to: "/")
    end
  
    defp signin(conn, changeset) do
      case insert_or_update_user(changeset) do
        {:ok, user} ->
          conn
          |> put_flash(:info, "Welcome back!")
          |> put_session(:user_id, user.id)
          |> redirect(to: page_path(conn, :index))
        {:error, reason} ->
          conn
          |> put_flash(:error, reason)
          |> redirect(to: "/")
      end
    end
  
    defp insert_or_update_user(changeset) do
      case Nota.Repo.get_by(Accounts.User, email: changeset.changes.email) do
        nil ->
          Nota.Repo.insert(changeset)
        user ->
          {:ok, user}
      end
    end
  end
  