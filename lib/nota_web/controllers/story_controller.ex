defmodule NotaWeb.StoryController do
  use NotaWeb, :controller

  alias Nota.Stories
  alias Nota.Stories.Story

  action_fallback NotaWeb.FallbackController

  def index(conn, _params) do
    stories = Stories.list_stories()
    render(conn, "index.json", stories: stories)
  end

  def create(conn, params) do
    {genre_id, map} = Map.pop(params, "genre_id")
    IO.inspect genre_id
    with {:ok, %Story{} = story} <- Stories.create_story(genre_id, map) do
      conn
      |> put_status(:created)
      |> put_resp_header("location", story_path(conn, :show, story))
      |> render("show.json", story: story)
    end
  end

  # def show(conn, %{"id" => id}) do
  #   genre = Misc.get_genre!(id)
  #   render(conn, "show.json", genre: genre)
  # end

  # def update(conn, %{"id" => id, "genre" => genre_params}) do
  #   genre = Misc.get_genre!(id)

  #   with {:ok, %Genre{} = genre} <- Misc.update_genre(genre, genre_params) do
  #     render(conn, "show.json", genre: genre)
  #   end
  # end

  # def delete(conn, %{"id" => id}) do
  #   genre = Misc.get_genre!(id)
  #   with {:ok, %Genre{}} <- Misc.delete_genre(genre) do
  #     send_resp(conn, :no_content, "")
  #   end
  # end
end
