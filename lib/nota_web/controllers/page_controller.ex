defmodule NotaWeb.PageController do
  use NotaWeb, :controller
  plug Ueberauth

  def index(conn, _params) do
    render conn, "index.html"
  end
end
