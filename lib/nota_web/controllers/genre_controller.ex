defmodule NotaWeb.GenreController do
  use NotaWeb, :controller

  alias Nota.Misc
  alias Nota.Misc.Genre

  action_fallback NotaWeb.FallbackController

  def index(conn, _params) do
    genres = Misc.list_genres()
    render(conn, "index.json", genres: genres)
  end

  def create(conn, params) do
    IO.inspect params
    with {:ok, %Genre{} = genre} <- Misc.create_genre(params) do
      conn
      |> put_status(:created)
      |> put_resp_header("location", genre_path(conn, :show, genre))
      |> render("show.json", genre: genre)
    end
  end

  def show(conn, %{"id" => id}) do
    genre = Misc.get_genre!(id)
    render(conn, "show.json", genre: genre)
  end

  def update(conn, %{"id" => id, "genre" => genre_params}) do
    genre = Misc.get_genre!(id)

    with {:ok, %Genre{} = genre} <- Misc.update_genre(genre, genre_params) do
      render(conn, "show.json", genre: genre)
    end
  end

  def delete(conn, %{"id" => id}) do
    genre = Misc.get_genre!(id)
    with {:ok, %Genre{}} <- Misc.delete_genre(genre) do
      send_resp(conn, :no_content, "")
    end
  end
end
