defmodule NotaWeb.Router do
  use NotaWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", NotaWeb do
    pipe_through :browser # Use the default browser stack

    get "/", PageController, :index
  end

  scope "/api", NotaWeb do
    pipe_through(:api)
    resources("/genres", GenreController)
    resources("/stories", StoryController)
  end

  scope "/auth", NotaWeb do
    pipe_through :browser
  
    get "/signout", AuthController, :signout
    get "/:provider", AuthController, :request
    get "/:provider/callback", AuthController, :callback
  end
  # Other scopes may use custom stacks.
  # scope "/api", NotaWeb do
  #   pipe_through :api
  # end
end
