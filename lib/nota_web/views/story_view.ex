defmodule NotaWeb.StoryView do
  use NotaWeb, :view
  alias NotaWeb.StoryView

  def render("index.json", %{stories: stories}) do
    %{data: render_many(stories, StoryView, "story.json")}
  end

  def render("show.json", %{story: story}) do
    %{data: render_one(story, StoryView, "story.json")}
  end

  def render("story.json", %{story: story}) do
    %{id: story.id,
      title: story.title,
      description: story.description,
      genre_id: story.genre_id
    }
  end
end
