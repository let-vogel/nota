defmodule Nota.StoriesTest do
  use Nota.DataCase

  alias Nota.Stories

  describe "stories" do
    alias Nota.Stories.Story

    @valid_attrs %{description: "some description", genre: 42, title: "some title"}
    @update_attrs %{description: "some updated description", genre: 43, title: "some updated title"}
    @invalid_attrs %{description: nil, genre: nil, title: nil}

    def story_fixture(attrs \\ %{}) do
      {:ok, story} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Stories.create_story()

      story
    end

    test "list_stories/0 returns all stories" do
      story = story_fixture()
      assert Stories.list_stories() == [story]
    end

    test "get_story!/1 returns the story with given id" do
      story = story_fixture()
      assert Stories.get_story!(story.id) == story
    end

    test "create_story/1 with valid data creates a story" do
      assert {:ok, %Story{} = story} = Stories.create_story(@valid_attrs)
      assert story.description == "some description"
      assert story.genre == 42
      assert story.title == "some title"
    end

    test "create_story/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Stories.create_story(@invalid_attrs)
    end

    test "update_story/2 with valid data updates the story" do
      story = story_fixture()
      assert {:ok, story} = Stories.update_story(story, @update_attrs)
      assert %Story{} = story
      assert story.description == "some updated description"
      assert story.genre == 43
      assert story.title == "some updated title"
    end

    test "update_story/2 with invalid data returns error changeset" do
      story = story_fixture()
      assert {:error, %Ecto.Changeset{}} = Stories.update_story(story, @invalid_attrs)
      assert story == Stories.get_story!(story.id)
    end

    test "delete_story/1 deletes the story" do
      story = story_fixture()
      assert {:ok, %Story{}} = Stories.delete_story(story)
      assert_raise Ecto.NoResultsError, fn -> Stories.get_story!(story.id) end
    end

    test "change_story/1 returns a story changeset" do
      story = story_fixture()
      assert %Ecto.Changeset{} = Stories.change_story(story)
    end
  end
end
