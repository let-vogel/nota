defmodule Nota.Repo.Migrations.CreateStories do
  use Ecto.Migration

  def change do
    create table(:stories) do
      add :title, :string
      add :description, :string
      add :genre_id, references(:genres, on_delete: :delete_all)

      timestamps()
    end

    create index(:stories, [:genre_id])
  end
end
