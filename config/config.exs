# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# General application configuration
config :nota,
  ecto_repos: [Nota.Repo]

# Configures the endpoint
config :nota, NotaWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "TnxM3nSfUguhxuP7fApzgO/Q2PT0Xj3hceU1LG40xNouAHxv+nAPFlVMDk8YCLzn",
  render_errors: [view: NotaWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: Nota.PubSub,
           adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:user_id]

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env}.exs"
