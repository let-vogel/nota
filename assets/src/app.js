import Vue from 'vue';

import Vuetify from 'vuetify';
import 'vuetify/dist/vuetify.min.css';
import router from './router';
import store from './store';

Vue.use(Vuetify)

import App from './App.vue'

new Vue({
  el: '#app',
  router,
  store,
  render: h => h(App)
})