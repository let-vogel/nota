import Vue from 'vue';
import Router from 'vue-router';

import MainDashboard from './components/dashboard/Main'
import New from './components/New'

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Dashboard',
      component: MainDashboard,
    },
    {
      path: '/new',
      name: 'New',
      component: New,
    }
  ],
});
