import Vue from 'vue';
import Vuex from 'vuex';
import axios from 'axios';
Vue.use(Vuex);

const store = new Vuex.Store({
    state: {
      genres: [],
      stories: []
    },
    actions: {
      get_stories(state) {
        axios.get('/api/stories').then((res) => {
          state.commit('set_state', {property: 'stories', payload: res.data.data})
        })
      },
      get_genres(state) {
        axios.get('/api/genres').then((res) => {
          state.commit('set_state', {property: 'genres', payload: res.data.data})
        })
      },
      add_story(state, story) {
        axios.post('/api/stories', story).then((res) => {
          state.dispatch('get_stories')
        })
      }
    },
    mutations: {
      set_state (state, {property, payload}) {
        state[property] = payload;
      }
    }
  })

  export default store